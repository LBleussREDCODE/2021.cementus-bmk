fetch("config.json")
.then(response => response.json())
.then(data => {
    console.log(data)
 
    //Hero-slider
    document.querySelector("#text01").innerHTML = data.content.hero.text;
    document.querySelector("#text02").innerHTML = data.content.hero.text2;
    document.querySelector("#text03").innerHTML = data.content.hero.text3;

    //Subline
    document.querySelector(".subline").innerHTML = data.content.subline.text;

    //article
    document.querySelector("#history").innerHTML = data.content.article.text;

    //images
    document.querySelector("#inlayText01").innerHTML = data.content.images.text;
    document.querySelector("#inlayText02").innerHTML = data.content.images.text2;
    document.querySelector("#inlayText03").innerHTML = data.content.images.text3;
    document.querySelector("#inlayText04").innerHTML = data.content.images.text4;
    document.querySelector("#inlayText05").innerHTML = data.content.images.text5;
    document.querySelector("#inlayText06").innerHTML = data.content.images.text6;

    //footer
    document.querySelector("#company").innerHTML = data.content.footer.company;
    document.querySelector("#ceo").innerHTML = data.content.footer.ceo;
    document.querySelector("#adress").innerHTML = data.content.footer.adress;

    document.querySelector("#telephone").innerHTML = data.content.footer.telephone;
    document.querySelector("#homepage").innerHTML = data.content.footer.homepage;
    document.querySelector("#email").innerHTML = data.content.footer.email;

    document.querySelector("#imprint").innerHTML = data.content.footer.imprint;
    document.querySelector("#legal").innerHTML = data.content.footer.legal;
    document.querySelector("#verwaltung").innerHTML = data.content.footer.verwaltung;
})



// external function https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_slideshow_auto modified
var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("hero-img-container");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 5000); 
}


//external function https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_scroll_to_top modified
var mybutton = document.getElementById("scrollup");


window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}


//external function https://www.w3schools.com/w3css/w3css_dropdowns.asp modified 
function dropFunction() {
  var x = document.getElementById("dropdown-table");
  if (x.className.indexOf("show") == -1) { 
    x.className += " show";
  } else {
    x.className = x.className.replace(" show", "");
  }
}

//phaser game
