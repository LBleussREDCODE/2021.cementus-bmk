var loadState = {

    preload: function() {

        var loadingLabel = game.add.text(80,150, 'Ladevorgang...',
                                        {font: '30px Courier', fill:'#ffffff'});

        game.load.image('background', '/assets/img/Bilder/background.png');
        game.load.image('platform', '/assets/img/Bilder/steelbeam.png');
        game.load.image('ground', '/assets/img/Bilder/ground.png');
        game.load.image('item', '/assets/img/Bilder/bag.png');
        game.load.image('game-logo', '/assets/img/Bilder/game-logo.png');
        game.load.spritesheet('pc', '/assets/img/Bilder/player-character.png', 32, 48);
        
        
    
    },
    create: function() {
        game.state.start('menu');
    }
};