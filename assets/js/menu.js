var menuState = {

    create: function (){
        var nameLabel = game.add.text(80,50, 'Cementus  Quest',
        {font: '64px "ArcadeClassic"', fill:'#000000'});

        game.add.sprite(200, 120, 'game-logo');
        game.stage.backgroundColor = "#82c9ed";

        var startLabel = game.add.text(80,game.world.height-80, 'DRÜCKE DIE "ENTER" TASTE UM ZU STARTEN.',
        {font: '15px Press Start 2P', fill:'#000000'}); //this font will not show up if the cache is emptied while reloading

        var enterkey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        enterkey.onDown.addOnce(this.start,this);
    },
    start: function()  {
        game.state.start('play');
    },
};